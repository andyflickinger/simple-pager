import gulp from 'gulp';
import connect from 'gulp-connect-php';
import browserSync from 'browser-sync';

gulp.task('connect-sync', function() {
  connect.server({}, function() {
    // filler
  });

  gulp.watch('**/*.php').on('change', function() {
    browserSync.reload();
  });
});
