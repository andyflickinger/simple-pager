// Smooth scroll internal links

export default () => {

  $('html').on('click', 'a[href*="#"]:not([href="#"])', (e) => {
    if (location.pathname.replace(/^\//, '') === e.currentTarget.pathname.replace(/^\//, '') && location.hostname === e.currentTarget.hostname) {
      let target = $(e.currentTarget.hash);
      target = target.length ? target : $('[id=' + e.currentTarget.hash.slice(1) + ']');
      if (target.length) {
        e.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top - 30
        }, 500);
        $(e.currentTarget).trigger('blur');
      }
    }
  });

};
