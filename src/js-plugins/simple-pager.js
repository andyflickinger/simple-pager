/* eslint-disable no-unused-vars */
/* global process */
class SimplePager {

  constructor(options = {
    offset: 2,
    current: 1,
    total: 1,
    contentContainer: $('.content-container'),
    endpoint: '',
    loader: $('.loader')
  }) {
    this.offset = options.offset;
    this.current = options.current;
    this.total = options.total;
    this.contentContainer = options.contentContainer;
    this.endpoint = options.endpoint || process.env.PAGER_API;
    this.loader = options.loader;

    // pagination arrows
    $('.pagination-next').on('click', (e) => {
      e.preventDefault();
      this.nextPage();
    });
    $('.pagination-prev').on('click', (e) => {
      e.preventDefault();
      this.prevPage();
    });

    this.registerClickEvents();
    this.checkParams(true);
    // this.getRequest(true);
  }

  renderPages(current, total) {
    this.current = current;
    this.total = total;
    // console.log(this);
    // clear pagination every time and rebuild
    $('.pagination-pages').html('');

    if (total > 1) {
      // always displaying first page and taking care of the previous arrow
      if (current === 1) {
        this.appendPage(1, true);
        $('.pagination-prev').attr('data-disabled', 'true');
      } else {
        this.appendPage(1);
        $('.pagination-prev').attr('data-disabled', 'false');
      }
      // loop for the middle pages, always showing a minimum number of pages before and after based on the offset
      if (total > 3 + (this.offset * 2)) {
        // display dots if more than a gap of the offset + first page + needed page for gap
        if (current > this.offset + 2) {
          this.appendPage(null, false, true);
        }
        // loops through current page plus/minus offset to add pages before and after current page
        for (let i = current - this.offset; i <= current + this.offset; i++) {
          if (i <= 1 || i >= total) {
            continue;
          } else {
            current === i ? this.appendPage(i, true) : this.appendPage(i);
          }
        }
        // display dots if less than a gap of the offset + last page + needed page for gap
        if (current <= total - (this.offset + 2)) {
          this.appendPage(null, false, true);
        }
      } else {
        for (let i = 2; i < total; i++) {
          current === i ? this.appendPage(i, true) : this.appendPage(i);
        }
      }
      // always displaying last page and hiding/showing next button depending on current page
      if (current === total) {
        this.appendPage(total, true);
        $('.pagination-next').attr('data-disabled', 'true');
      } else {
        this.appendPage(total);
        $('.pagination-next').attr('data-disabled', 'false');
      }

    } else {
      this.appendPage(1, true);
      $('.pagination-next').attr('data-disabled', 'true');
      $('.pagination-prev').attr('data-disabled', 'true');
    }

    // mobile counter
    $('.pagination-mobile-current').html(current);
    $('.pagination-mobile-total').html(total);

  }

  appendPage(num, current = false, dots = false) {
    const pagesContainer = $('.pagination-pages');
    if (dots) {
      pagesContainer.append('<span class="pagination-pages-dots">...</span>');
    } else {
      const dataCurrent = current ? ' data-current="true"' : ' data-current="false"';
      const template = `<a href="#"${dataCurrent}><span class="sr-text">Page</span>${num}</a>`;
      pagesContainer.append(template);
    }
  }

  getRequest(first = false) {
    this.contentContainer.html('');
    this.loader.show();
    const url = this.buildUrl();

    // added some scrolljacking for when filters are changed on non mobile devices
    // if (!first && $(window).width() > 599) {
    //   $('html, body').animate({
    //     scrollTop: (this.contentContainer.offset().top - 115)
    //   }, 500);
    // }

    $.get(url).then((data) => {
      // console.log(data);
      data.response.map((item) => {
        this.templateResults(item);
      });
      this.total = data.meta.total_pages;
      // console.log(this.current, this.total);
      this.renderPages(this.current, this.total);
      this.loader.hide();

    });
  }

  buildUrl() {
    // let services = '',
    //     alpha = '',
    //     keyword = '',
    //     page = '';
    const params = [];

    // if (providerFilterParams.services.length) {
    //   services = 'services=' + providerFilterParams.services.join(',');
    //   params.push(services);
    // }
    //
    // if (providerFilterParams.alpha) {
    //   alpha = 'alpha=true';
    //   params.push(alpha);
    // }

    if (this.current > 1) {
      // page = 'page=' + providerFilterParams.page;
      params.push('page=' + this.current);
    }

    // if (providerFilterParams.keyword.length) {
    //   keyword = 'keyword=' + providerFilterParams.keyword;
    //   params.push(keyword);
    // }
    // console.log(window);
    history.pushState('', '', window.location.pathname + '?' + params.join('&'));
    const url = `${this.endpoint}?${params.join('&')}`;
    // console.log(url);
    return url;
  }

  templateResults(item = false) {
    if (item) {
      const html = `
      <div class="pagination-item">
      <h4>${item.title}</h4>
      <p>${item.content}</p>
      </div>
      `;
      this.contentContainer.append(html);
    }
  }

  checkParams(first = false) {
    let params = window.location.search;
    if (params.length > 1) {
      params = params.substring(1);
      const paramArray = params.split('&');
      paramArray.forEach((pair) => {
        const key = pair.split('=')[0];
        const val = pair.split('=')[1];
        if (key === 'page') {
          this.current = parseInt(val);
        }
      });
    }
    this.getRequest(first);
  }

  registerClickEvents() {
    $(document).on('click', '.pagination-pages a', (e) => {
      e.preventDefault();
      const current = $(e.currentTarget)[0].dataset.current;
      if (current === 'true') {
        return false;
      } else {
        this.current = parseInt($(e.currentTarget)[0].lastChild.data);
        // return this.renderPages(this.current, this.total);
        return this.getRequest();
      }
    });
  }

  nextPage() {
    this.current += 1;
    // this.renderPages(this.current, this.total);
    this.getRequest();
  }

  prevPage() {
    this.current -= 1;
    // this.renderPages(this.current, this.total);
    this.getRequest();
  }
}

export default SimplePager;
