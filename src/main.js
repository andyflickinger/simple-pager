// -----------------------------------------------------------------------------
// Component Includes
// -----------------------------------------------------------------------------
import siteHeader from './components/site-header/site-header';
import formValidation from './js-plugins/form-validation';
import smoothScroll from './js-plugins/smooth-scroll';
import pagination from './components/pagination/pagination';
// import SimplePager from './js-plugins/simple-pager';

(($) => {

  // Document Ready
  $(() => {

    // console.log(SimplePager);
    // const pager = new SimplePager();
    // console.log(pager);
    // -----------------
    // High-priority JS
    // -----------------
    siteHeader();
    pagination();

    // -----------------
    // Low-priority JS
    // -----------------
    formValidation();
    smoothScroll();


  });

})($);
