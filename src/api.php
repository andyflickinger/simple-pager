<?php
header("Content-Type:application/json");
header('Access-Control-Allow-Origin: *');

$string = file_get_contents("api/data.json");
$data_array = json_decode($string, true);

$page = isset($_GET['page']) ? $_GET['page'] : 1;
$per_page = 10;
$total = count($data_array['data']);

$total_pages = ceil($total / $per_page);
$page_data = array();
$loop_start = ($page - 1) * $per_page;

for ($i = $loop_start; $i !== $loop_start + $per_page; $i++) {
  $page_data[] = $data_array['data'][$i];
}


$response = array (
  "response" => $page_data,
  "meta" => array (
    "total_pages" => $total_pages,
    "total" => $total
  )
);


$json_response = json_encode($response);
echo $json_response;
