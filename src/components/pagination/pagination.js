import SimplePager from '../../js-plugins/simple-pager';
export default() => {

  // const pager = new SimplePager({
  //   offset: 2
  // });

  const pager = new SimplePager();

  pager.templateResults = function(item = false) {
    if (item) {
      const html = `
      <div class="pagination-item">
        <h4>${item.title ? item.title : ''}</h4>
        <p>${item.content ? item.content : ''}</p>
        <hr>
      </div>
      `;
      this.contentContainer.append(html);
    }
  };

};
