# Simple Pager

Simplified AJAX pagination plugin


## Prerequisites

| Name   | Version    | Documentation                              |
|--------|------------|--------------------------------------------|
| Node   | >= 6       | [http://nodejs.org](http://nodejs.org)     |
| Yarn   | >= 1.5.1   | [https://yarnpkg.com](https://yarnpkg.com) |

## Setup

After you have Node and Yarn installed, you may begin to install the project's dependencies.

Install Node Packages using [Yarn](https://yarnpkg.com/en/):

```sh
$ yarn
```

Install Gulp Globally:

```sh
$ yarn global add gulp
```

There is an `env.example` file with all possible environment variables. These **MUST** be set for Gulp to build the project. Duplicate this file **(DO NOT DELETE or RENAME)** and rename it to `.env`.

To run Gulp to build the project:

```shell
$ gulp
```
---

## Core Files

Built off of Falcore, so everything is the same except for the files below:

```
gulp-tasks/ __________________________________
|  |- php.js _________________________________ # New gulp task for our PHP API
|- api/ ______________________________________
|  |- data.json ______________________________ # Mock API Data
|- components/ _______________________________
|  |- pagination/ ____________________________ # Individual Component
|    |- pagination.js ________________________ # JS file where we instantiate the pager and pass any options, customizations
|    |- pagination.scss ______________________ # Pagination starter styles
|    |- pagination.twig ______________________ # Pagination template
|- js-plugins/ _______________________________
|  |- simple-pager.js ________________________ # Plugin file
|- pages/ ____________________________________
|  |-index.twig ______________________________ # Demo Page
```

## Using

Import the plugin file in your components JS file:

```js
import SimplePager from '../../js-plugins/simple-pager';
```

Instantiate it

```js
const pager = new SimplePager();
```

Change up the options, below are the defaults:

```js
const pager = new SimplePager({
  offset: 2,  
  current: 1,
  total: 1,
  contentContainer: $('.content-container'),
  endpoint: '',
  loader: $('.loader')
})
```
---

###### Offset
- sets the number of adjacent pages that show next to the current page

###### Current
- sets current page to start on (probably not that useful, but gets updated from the AJAX call)

###### Total
- sets total number of pages (also updated by AJAX call)

###### Content Container
- container that the page results get appended to, default is included in the pagination components

###### Endpoint
- URL for the AJAX call

###### Loader
- container that holds the loading animation, this gets shown while the AJAX call is working and hidden when the results are returned

---

#### Author

Interactive Strategies - [frontend@interactivestrategies.com](frontend@interactivestrategies.com)

---

#### License

This project is licensed under the MIT License - see the `LICENSE.md` file for details
